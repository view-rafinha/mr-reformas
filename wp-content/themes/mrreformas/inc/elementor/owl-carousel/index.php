<?php 
class OMElementorOWLCarouselWidget extends \Elementor\Widget_Base {

	public function __construct($data = [], $args = null)
	{
		parent::__construct($data, $args);
		// wp_register_style( 'swiper-slide', 'https://unpkg.com/swiper@8/swiper-bundle.min.css', [], '1.0.0' );
		wp_register_script( 'owl-carousel', get_stylesheet_directory_uri().'/assets/js/owl.carousel.min.js', [ 'elementor-frontend', 'jquery' ], '1.0.0', true );
		wp_register_script( 'owl-carousel-scripts', get_stylesheet_directory_uri().'/inc/elementor/owl-carousel/js/scripts.js', [ 'elementor-frontend', 'jquery' ], '1.0.0', true );
	}

	// public function get_style_depends() {
	// 	// return [ 'swiper-slide' ];
	// }

	public function get_script_depends() {
		return [ 'owl-carousel','owl-carousel-scripts' ];
	}

	public function get_name() {
        return 'OMElementorOWLCarouselWidget';
    }

	public function get_title() {
        return 'OWL Carrossel';
    }

	public function get_icon() {
        return 'eicon-testimonial-carousel';
    }

	protected function register_controls() {
        $this->start_controls_section(
			'content_section',
			[
				'label' => __( 'Conteúdo', 'CSPScriptBannerWidget' ),
				'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
			]
		);

        $repeater = new \Elementor\Repeater();

        $repeater->add_control(
			'icon',
			[
				'type' => \Elementor\Controls_Manager::MEDIA,
				'label_block' => true,
				'label' => __( 'Ícone', 'CSP' ),
				'dynamic' => [
					'active' => true,
				],
			]
		);

        $repeater->add_control(
			'title',
			[
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'label' => __( 'Título', 'CSP' ),
				'dynamic' => [
					'active' => true,
				],
			]
		);

        $repeater->add_control(
			'text',
			[
				'type' => \Elementor\Controls_Manager::WYSIWYG,
				'label_block' => true,
				'label' => __( 'Texto', 'CSP' ),
				'dynamic' => [
					'active' => true,
				],
			]
		);

        $this->add_control(
			'items',
			[
				'show_label' => false,
				'type' => \Elementor\Controls_Manager::REPEATER,
				'fields' => $repeater->get_controls(),
				'title_field' => '{{{ title }}}'
			]
		);

        $this->end_controls_section();

    }

	protected function render() {
        $settings = $this->get_settings_for_display();

	?>
		<div class="owl-carousel">
            <?php 
            foreach($settings['items'] as $item) {
            ?>
                <div class="item">
                    <div class="icon"><img src="<?php echo $item['icon']['url'] ?>" alt="<?php echo $item['title'] ?>"></div>
                    <div class="title"><?php echo $item['title'] ?></div>
                    <div class="text"><?php echo $item['text'] ?></div>
                </div>
            <?php 
            }
            ?>
        </div>
	<?php
    }

}

?>