<?php 
add_action( 'customize_register', function($wp_customize){

    //   HEADER LOGO
      $wp_customize->add_setting( 'header_logo', array(
        'type' => 'theme_mod', // or 'option'
        'capability' => 'edit_theme_options',
        'theme_supports' => '', // Rarely needed.
        'default' => '',
        'transport' => 'postMessage', // or postMessage
        'sanitize_callback' => '',
        'sanitize_js_callback' => '', // Basically to_json.
      ) );

      $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'header_logo', array(
        'label' => 'Logo',
        'priority' => 20,
        'section' => 'title_tagline',
        'settings' => 'header_logo',
        'description' => 'Defina a logo do seu site, a mesma será exibida no cabeçalho',
        'button_labels' => array(// All These labels are optional
                    'select' => 'Selecionar Logo',
                    'remove' => 'Remover Logo',
                    'change' => 'Definir Logo',
                    )
      )));

      // ADD SECTION 
      $wp_customize->add_section( 'footer', array(
 
        'title'=> __( 'Rodapé', 'mrreformas' ),
        'priority' => 201
     ) );

      // LOCATION
      $wp_customize->add_setting( 'footer_location');
      $wp_customize->add_control('header_logo', array(
        'type' => 'textarea',
        'label' => __('Localização', 'mrreformas'),
        'priority' => 20,
        'section' => 'footer',
        'settings' => 'footer_location',
        'description' => 'Informe o endereço de sua empresa',
      ));

      // CONTACT
      $wp_customize->add_setting( 'footer_contact');
      $wp_customize->add_control('footer_contact', array(
        'type' => 'textarea',
        'label' => __('Contato', 'mrreformas'),
        'priority' => 20,
        'section' => 'footer',
        'settings' => 'footer_contact',
        'description' => 'Informe os dados de contato da sua empresa.',
      ));

      // SOCIAL INSTAGRAM
      $wp_customize->add_setting( 'footer_social_instagram');
      $wp_customize->add_control('footer_social_instagram', array(
        'type' => 'url',
        'label' => __('Instagram', 'mrreformas'),
        'priority' => 20,
        'section' => 'footer',
        'settings' => 'footer_social_instagram',
        'description' => 'Informe a url do instagram de sua empresa.',
      ));

      // SOCIAL INSTAGRAM
      $wp_customize->add_setting( 'footer_social_whatsapp');
      $wp_customize->add_control('footer_social_whatsapp', array(
        'label' => __('Whastsapp', 'mrreformas'),
        'priority' => 20,
        'section' => 'footer',
        'settings' => 'footer_social_whatsapp',
        'description' => 'Informe o número de whatsapp da sua empresa no seguinte formato código do pais+ddd+número. Exemplo: 5548984627444',
      ));

} );

?>