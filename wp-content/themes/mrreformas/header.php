<!DOCTYPE html>

<html class="no-js" <?php language_attributes(); ?>>

	<head>

		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1.0" >

		<link rel="profile" href="https://gmpg.org/xfn/11">

        <script src="https://kit.fontawesome.com/1f71fdcc17.js" crossorigin="anonymous"></script>
		<?php wp_head(); ?>
    
	</head>

	<body <?php body_class(); ?>>

		<?php
		wp_body_open();
		?>

        <header>
            <div class="logo">
                <?php 
                    $header_logo = get_theme_mod('header_logo');
                    if(!empty($header_logo)) {
                ?>
                    <a href="<?php echo site_url(); ?>">
                        <img src="<?php echo $header_logo ?>" alt="<?php echo get_bloginfo() ?>">
                    </a>
                <?php 
                    }
                ?>
                
            </div>
            <div class="menu">
                <?php 
                if ( has_nav_menu( 'header_menu' ) ) {
                    wp_nav_menu(
                        array(
                            'container'  => '',
                            'items_wrap' => '%3$s',
                            'theme_location' => 'header_menu',
                        )
                    );
                }
                ?>
            </div>
            <div class="button">
                <a href="#">
                   SOLICITAR ORÇAMENTO
                </a>
            </div>
        </header>