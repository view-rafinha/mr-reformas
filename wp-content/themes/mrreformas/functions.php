<?php 
require_once('inc/wp_customize.php');
require_once('inc/elementor/index.php');

add_theme_support( 'title-tag' );
add_action( 'wp_head', function() {
    echo "<title>";
        wp_title( '|', true, 'right' );
    echo "</title>";
});

// Register a menu
add_action( 'after_setup_theme', function(){
    register_nav_menus( array(
        'header_menu' => __( 'Cabeçalho', 'alquimya' ),
        'footer_menu'  => __( 'Rodapé', 'alquimya' ),
    ) );
}, 0 );


// Add styles and script theme
add_action( 'wp_enqueue_scripts', function(){
    wp_enqueue_style( 'owl-carousel', get_template_directory_uri().'/assets/css/owl.carousel.min.css', [], time() );
    wp_enqueue_style( 'dashicons' );
    wp_enqueue_style( 'style', get_stylesheet_uri(), [], time() );
    // wp_enqueue_script( 'script-alquimya', get_template_directory_uri() . '/assets/js/scripts.js', array(), time(), true );
} );


?>