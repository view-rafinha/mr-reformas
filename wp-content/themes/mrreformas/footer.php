<footer>
    <div class="footer-top container">
        <div class="logo">
            <?php 
                $header_logo = get_theme_mod('header_logo');
                if(!empty($header_logo)) {
            ?>
                <a href="<?php echo site_url(); ?>">
                    <img src="<?php echo $header_logo ?>" alt="<?php echo get_bloginfo() ?>">
                </a>
            <?php 
                }
            ?>
            
        </div>
        <div class="menu">
            <?php 
            if ( has_nav_menu( 'header_menu' ) ) {
                wp_nav_menu(
                    array(
                        'container'  => '',
                        'items_wrap' => '%3$s',
                        'theme_location' => 'header_menu',
                    )
                );
            }
            ?>
        </div>
        
    </div>
    <hr class="container">
    <div class="footer-bottom container">
        <div class="left">
            <h2>Localização</h2>
            <?php 
            $location = get_theme_mod('footer_location');
            echo $location;
            ?>
        </div>
        <div class="center">
            <h2>Contato</h2>
            <?php 
            $contact = get_theme_mod('footer_contact');
            echo $contact;
            ?>
        </div>
        <div class="right">
            <?php 
            $instagram = get_theme_mod('footer_social_instagram');
            $whatsapp = get_theme_mod('footer_social_whatsapp');
            ?>
            <h2>Nossas Redes</h2>
            <a href="<?php echo $instagram ?>" target="_blank" class="instagram"><span class="dashicons dashicons-instagram"></span></a>
            <a href="https://wa.me/<?php echo $whatsapp ?>" target="_blank" class="whatsapp"><span class="dashicons dashicons-whatsapp"></span></a>
        </div>
    </div>
    
</footer>

<?php wp_footer(); ?>

</body>